% Accordance = Partner(A) teil von Start(A')

% Operating Guideline, um Accordance zu prüfen. 

% OG-Matching
% Theorem Austauschbarkeit: Wenn OG(A) matcht OG(A') dann Partner(A) teil von Start(A'). 

Die Public View einer Partei beschreibt das Außenverhalten des zu implementierenden Service. Die tatsächliche Implementation kann allerdings von der Public View abweichen. Wir beschäftigen uns daher mit der Fragestellung, wann die Public View durch eine abweichende Implementation ("Private View") ausgetauscht werden kann, ohne dabei die Deadlock-Freiheit des Contracts zu beeinflussen. Wir werden außerdem einen Algorithmus definieren mit dem Austauschbarkeit überprüft werden kann. 

\begin{definition}[Austauschbarkeit]
\label{austauschbarkeit}
Ein offenes Netz $N$ ist durch ein offenes Netz $N'$ austauschbar gdw. $N$ und $N'$ ihre Schnittstellen gleich sind ($I_N = I_{N'}$ und $O_N = O_{N'}$) und N' mindestens alle Partner von N hat, also $Partner(N') \supseteq Partner(N)$.
\end{definition}

Die Menge aller Partner $Partner(N)$ eines offenen Netzes $N$ ist im Allgemeinen unendlich groß. Um Austauschbarkeit zu entscheiden, ist es nach Def. \ref{austauschbarkeit} also nötig die Teilmengenbeziehung von im Allgemeinen unendlichen großen Mengen zu prüfen. Wir wollen daher eine endliche Repräsentation der Menge $Partner(N)$ einführen, die Operating Guideline. 

\begin{definition}[Annotierter Automat]
$A^\Phi = [Q, C, \delta, q_0, \Phi]$ ist ein annotierter Automat gdw. 
\begin{itemize}
	\item $Q$ eine nichtleere endliche Menge von Zuständen ist, 
	\item $C$ eine Menge von Labels ist, 
	\item $\delta \subseteq Q \times C \times Q$ eine Zustandsübergangsrelation ist so dass jeder Zustand 
		$q \in Q$ von $q_0$ durch transitives Anwenden von $\delta$ erreichbar ist, 
	\item $q_0 \in Q$ der Startzustand ist, und
	\item $\Phi : Q \to \mathcal{BF}$ eine Abbildung, die jedem Zustand eine Annotation zuordnet, d.h. 
		für alle $q \in Q$ ist $\Phi (q)$ eine Boolsche Formel mit Literalen aus $C$. 
\end{itemize}
\end{definition}

Ein annotierter Automaten repräsentieren eine Menge von offenen Netzen. 

Für ein offenes Netz $M$ mit Schnittstelle $I \cup O$ und einen annotierten Automaten $A^\Phi$, mit Boolschen 
Formeln über diesen Schnittstellenplätzen und einem speziellen Literal $final$ (also $C = I \cup O \cup \{ final \}$), 
definieren wir wann $M$ mit $A^\Phi$ matched. 

Intuitiv matched $M$ mit $A^\Phi$ wenn das Verhalten von $M$ durch $A^\Phi$ simuliert wird und jede durch 
$A^\Phi$ erreichte Markierung von $M$ die entsprechende Formel von $A^\Phi$ erfüllt. 

Der Einfachheit halber sollen nachfolgende offene Netze normal sein, d.h. jede Transition soll mit maximal einem 
Schnittstellenplatz verbunden sein. Nach \cite{VanDerAalst2010} ist das eine triviale Forderung, da jedes 
offene Netz in ein äquivalentes normales offenes Netz übersetzt werden kann. 

\begin{definition}[Matching]
Sei $M$ ein normales offenes Netz und sei $X$ die Menge aller Markierungen des Petrinetzes $M^*$, was aus $M$ entsteht, 
in dem alle Schnittstellenplätze von $M$ entfernt werden. Sei $A^\Phi = [Q, C, \delta, q_0, \Phi]$ ein annotierter 
Automat mit $C = I_M \cup O_M \cup \{final\}$. 

Dann matched $M$ mit $A^\Phi$ gdw. eine Abbildung $\rho : X \to Q$ mit folgenden Eigenschaften existiert: 
\begin{enumerate}
	\item $\rho ({m_0}_M) = q_0$. 
	\item Falls $t \in T_M$ eine interne Transition ist (t ist mit keinem Schnittstellenplatz verbunden), 
		$m, m' \in X$, und $m \overset{t}{\longrightarrow}_M m'$, dann ist $\rho(m) = \rho(m')$.
	\item Falls $t \in T_M$ eine empfangende Transition ist mit $c \in I_M$, $c \in {}^\bullet t$, $m, m' \in X$, und 
		$(m + [c]) \overset{t}{\longrightarrow}_M m'$, dann existiert ein Zustand $q' \in Q$ mit 
		$(\rho(m), c, q') \in \delta$ und $\rho(m') = q'$.
	\item Falls $t \in T_M$ eine sendende Transition ist mit $c \in O_M$, $c \in t^\bullet$, $m, m' \in X$, und 
		$m \overset{t}{\longrightarrow}_M (m' + [c])$, dann existiert ein Zustand $q' \in Q$ mit 
		$(\rho(m), c, q') \in \delta$ und $\rho(m') = q'$.
	\item Für alle $m \in X$ mindestens eine der folgende Eigenschaften ist erfüllt:
		\begin{enumerate}
			\item Eine interne Transition $t$ ist in $m$ aktiviert, oder
			\item $\Phi(\rho(m))$ evaluiert zu $true$ unter folgender Belegung $\beta$: 
				\begin{enumerate}
					\item $\beta(c) = true$, falls $c \in O_M$ und es gibt eine Transition $t$ mit $c \in t^\bullet$, die in $m$ aktiviert ist.
					\item $\beta(c) = true$, falls $c \in I_M$ und es gibt eine Transition $t$ mit $c \in {}^\bullet t$, die in $m + [c]$ aktiviert ist.
					\item $\beta(c) = true$, falls $c = final$ und $m \in \Omega_M$.
					\item $\beta(c) = false$ in allen anderen Fällen.
				\end{enumerate}
		\end{enumerate}
\end{enumerate} 
\end{definition}

\begin{definition}[Operating Guideline]
Ein annotierter Automat heißt Operating Guideline $OG_N$ eines offenen Netzes $N$ gdw. $Partner(N)$ genau die Menge aller offenen Netze ist, die mit $OG_N$ matchen.
\end{definition}

Mit der Operating Guideline $OG_N$ haben wir nun eine endliche Repräsentation von $Partner(N)$. Wir betrachten nun einen Weg, Austauschbarkeit von zwei Services über ihre Operating Guidelines zu entscheiden. Dafür führen wir eine Simulationsrelation $\sqsubseteq$ ein, die zusätzlich in jedem Zustand Prüft, ob die Annotation von $OG_N$ die von $OG_{N'}$ impliziert. 

\begin{definition}[$\sqsubseteq$-Relation von OGs]
Seien $N$ und $N'$ zwei offene Netze mit äquivalenter Schnittstelle, und $OG_N = [Q, C, \delta, q_0, \Phi]$ und $OG_{N'} = [Q', C', \delta', q_0', \Phi']$ ihre Operting Guidelines. 
Dann ist $OG_N \sqsubseteq OG_{N'}$ gdw. es existiert eine Simulationsrelation $\xi \subseteq Q \times Q'$ mit 
\begin{enumerate}
	\item $\xi (q_0) = q_0'$, 
	\item wenn $\xi (q) = q'$ und $(q, c, q_1) \in \delta$, dann existiert ein $q_1'$ so dass $(q', c, q_1') \in \delta'$ und $xi (q_1) = q_1'$, und 
	\item für alle $(q, q') \in \xi$ ist die Formel $\Phi(q) \Rightarrow \Phi'(q')$ eine Tautologie. 
\end{enumerate}
\end{definition}

\begin{satz}[$\sqsubseteq$-Relation ist äquivalent zu Austauschbarkeit]
Seien $N$ und $N'$ zwei offene Netze, und $OG_N$ und $OG_{N'}$ ihre Operating Guidelines. Dann ist $N$ austauschbar durch $N'$  gdw. $OG_N \sqsubseteq OG_{N'}$.
\end{satz}

Dieser Satz wird in \cite{Stahl2009} mit einem anderen Servicemodell, den Serviceautomaten, bewiesen. 

Während in dieser Arbeit ein Partner nur eine deadlock-freie Komposition ermöglichen muss, wird der Begriff Austauschbarkeit in \cite{VanDerAalst2010} für deadlock-freie und livelock-freie Partner bewiesen. Dort gilt allerdings die Einschränkung auf azyklische und endliche offene Netze. 



